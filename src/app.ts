import express from 'express'
import config from './config'
import videoRoutes from './routes/videos.routes'
import userRoutes from './routes/user.route'
import cors from 'cors'
import morgan from 'morgan'
import bodyParser from 'body-parser'
const app = express()
app.use(express.urlencoded({ extended: false }))
// parse application/json
app.use(express.json())
app.set('port', config.PORT || 3004)
app.use(morgan('dev'))
app.use(cors())
app.use(videoRoutes)
app.use(userRoutes)
app.use(express.json())
app.use(express.urlencoded({ extended: false }))

export default app
