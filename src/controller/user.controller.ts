import { RequestHandler } from 'express'
import { validationResult } from 'express-validator'
import User from '../model/User'
import bcrypt from 'bcryptjs';
import generarJWT from '../helpers/jwt';

export const registro: RequestHandler = async (req, res) => {
    const { name, lastName, email, password } = req.body

    try {
        // 
        let user: any = await User.findOne({ email: email })
        console.log(user);

        if (user) {
            return res.status(400).json({
                status: 'FAIL',
                msg: "El correo ya se ha registrado antes"
            })
        }
        // Encriptar pass
        const salt = bcrypt.genSaltSync()
        const userSave: any = new User(req.body)
        userSave.password = bcrypt.hashSync(password, salt)

        // Guardar usuario
        await userSave.save()

        // Generar JWT
        const token = await generarJWT(userSave.id, userSave.name)
        res.status(201).json({
            status: 'OK',
            msg: "Se ha registrado",
            uid: userSave.id,
            name: userSave.name,
            token: token
        })
    } catch (error) {
        console.log(error);
        res.status(500).json({
            status: 'FAIL',
            msg: 'Comuniquese con el administrador',

        })
    }
}

export const login: RequestHandler = async (req, res) => {

    const { email, password } = req.body
    try {
        // verificar si existe el usuario
        let user: any = await User.findOne({ email: email })
        console.log(user);

        if (!user) {
            return res.status(400).json({
                status: 'FAIL',
                msg: "Este email no esta reguistrado"
            })
        }

        // Canfirmar los passwords
        const validPasswords = bcrypt.compareSync(password, user.password)
        if (!validPasswords) {
            return res.status(400).json({
                status: 'FAIL',
                msg: "Password incorrecto"
            })
        }

        //generar JWT
        const token = await generarJWT(user.id, user.name)


        res.json({
            status: "OK",
            uid: user.id,
            name: user.name,
            lastName: user.lastName,
            token

        })
    } catch (error) {
        console.log(error);
        res.status(500).json({
            status: 'FAIL',
            msg: 'Comuniquese con el administrador'
        })
    }
}


export const verificarToken: RequestHandler = async (req: any, res) => {
    // console.log('renew');
    //  x-tokens headers
    const uid = req.uid
    const name = req.name
    res.json({
        status: 'OK', uid, name
    })
}