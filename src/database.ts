import mongoose, { ConnectionOptions } from 'mongoose'
import config from './config'
;(async () => {
  try {
    // resuelve advertencia
    mongoose.set('useCreateIndex', true)
    const mongooseOptions: ConnectionOptions = {
      useNewUrlParser: true,
      useUnifiedTopology: true
      //   user: config.MONGO_USER,
      //   pass: config.MONGO_PASSWORD
    }
    const db = await mongoose.connect(
      `mongodb://${config.MONGO_HOST}/${config.MONGO_DATABASE}`,
      mongooseOptions
    )

    console.log('database connected to:', db.connection.name)
  } catch (error) {
    console.log(error)
  }
})()
