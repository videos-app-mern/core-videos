import { RequestHandler } from "express";
import jwt from 'jsonwebtoken';
const validarJWT: RequestHandler = (req: any, res, next) => {
    const token = req.header('x-token')
    console.log(token);
    if (!token) {
        return res.json(401).json({
            status: 'FAIL',
            msg: "Not Token provider"
        })
    }

    try {
        const payload: any = jwt.verify(token, 'APP-TOKEN-JWT-SECRET')
        console.log(payload);
        req.uid = payload.uid
        req.name = payload.name
    } catch (error) {
        return res.status(401).json({
            status: 'FAIL',
            msg: "Token no valido"
        })
    }
    next()
}

export default validarJWT