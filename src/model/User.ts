import { Schema, model } from 'mongoose'
import { truncate } from 'node:fs'

const userSchema = new Schema(
    {
        name: {
            type: String,
            required: true,
            trim: true
        },
        lastName: {
            type: String,
            required: true,
            trim: true
        },

        email: {
            type: String,
            trim: true,
            required: true,
            unique: true
        },
        password: {
            type: String,
            required: true,
            trim: true
        }
    },
    {
        versionKey: false,
        timestamps: true
    }
)

export default model('User', userSchema)
